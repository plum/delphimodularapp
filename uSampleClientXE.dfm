object FSimpleClientXE: TFSimpleClientXE
  Left = 0
  Top = 0
  Caption = 'FSimpleClientXE'
  ClientHeight = 73
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnLoadDelphi5Plugin: TButton
    Left = 8
    Top = 8
    Width = 281
    Height = 25
    Caption = 'btnLoadDelphi5Plugin'
    TabOrder = 0
    OnClick = btnLoadDelphi5PluginClick
  end
  object btnLoadDelphiXEPlugin: TButton
    Left = 8
    Top = 39
    Width = 281
    Height = 25
    Caption = 'btnLoadDelphiXEPlugin'
    TabOrder = 1
    OnClick = btnLoadDelphiXEPluginClick
  end
end
