library libSamplePlugIn;

uses
  //ShareMemRep,
  ShareMem,
  Windows,
  Classes,
  uPlugInIntf in 'uPlugInIntf.pas',
  uSamplePlugInIntf in 'uSamplePlugInIntf.pas',
  uSamplePlugInImpl in 'uSamplePlugInImpl.pas';

{$E plg}  //
{$R *.res}

procedure DllMain(Reason: Integer);
begin
  case Reason of
   { our 'dll' will be unloaded immediantly, so free up the shared
     datamodule, if created before! }
    DLL_PROCESS_DETACH:
     { place your code here! }
  end;
end;

{-----------------------------------------------------------------------------
  Procedure: RegisterPlugIn
  Arguments: AOwner: TComponent; ForceCreate: Boolean = False
  Result:    TSamplePlugIn
-----------------------------------------------------------------------------

  Description:

  'RegisterPlugIn' is the one-and-only exported dll function to register a
  plugin. This method will be called from the plugin-manager used by the
  host to load and register the underlying plugin interface.

-----------------------------------------------------------------------------}

function RegisterPlugIn(AOwner: TComponent;
  ForceCreate: Boolean = False): TSamplePlugIn; stdcall;
begin
  Result := TSamplePlugIn.Create(AOwner);
end;

exports RegisterPlugIn;

{ ---------------------------------------------------------------------------

  To be able to initialize some other stuff before registering the plugin
  (e.g. creating a datamodule), we're hooking the load/unload-process of the
  dll by deligating the necessary calls to our own method.

-----------------------------------------------------------------------------}

begin
  { attach our own dll loader }
  //DllProc := @DllMain;
  //DllProc(DLL_PROCESS_ATTACH);

  //SysReallocMem(DllProc, GetMem(DllProc));
end.
