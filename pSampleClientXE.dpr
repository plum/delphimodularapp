program pSampleClientXE;

uses
  Vcl.Forms,
  uSampleClientXE in 'uSampleClientXE.pas' {FSimpleClientXE};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFSimpleClientXE, FSimpleClientXE);
  Application.Run;
end.
