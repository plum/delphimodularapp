unit uSamplePlugInIntf;

interface

uses
  uPlugInIntf, Classes;

const
  CS_NAME    = 'SamplePlugIn';
  CS_VERSION = '1.0.0';

type
{ ISamplePlugIn }

  ISamplePlugIn = interface(ICustomPlugIn)
  ['{14952C19-FAAA-438A-8100-3F56F79FD4B1}']
    function Sum(a, b: Integer): Integer;
  end;

implementation

end.
