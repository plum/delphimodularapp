unit uPlugInIntf;

interface

uses
  Classes;

type

{ TPlugInClass }

  TPlugInClass = class(TComponent);

{ TPlugInProc }

  TPlugInProc = function(AOwner: TComponent;
    ForceCreate: Boolean = False): TPlugInClass; stdcall;


{ ICustomPlugIn }

  ICustomPlugIn = interface
  ['{AB3807B7-759C-4ABA-B794-2C8E55E8F785}']
    function GetAuthor: string;
    function GetName: string;
    function GetDescription: string;
    function GetVersion: string;
  end;

implementation

end.
