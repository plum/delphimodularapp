unit uSampleClient;

interface

uses
  //ShareMemRep,
  ShareMem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  uPlugInMng,
  uSamplePlugInIntf,
  StdCtrls;

type
  TFSampleClient = class(TForm)
    btnLoadPlugin: TButton;
    btnLoadPluginXE: TButton;
    procedure btnLoadPluginClick(Sender: TObject);
    procedure btnLoadPluginXEClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LoadPlugin(PluginPath: string);
  end;

var
  FSampleClient: TFSampleClient;

  PlugInManager: TPlugInManager;
  SamplePlugIn: ISamplePlugIn;
  FileName: string;
  isLoaded: Boolean;

resourcestring
    RS_PLUGIN_ERROR =
      'Error while loading plugin "%s"!';

implementation

{$R *.DFM}

procedure TFSampleClient.LoadPlugin(PluginPath: string);
begin
  { initialize variables }
  isLoaded := False;

  { create plugin manager }
  PlugInManager := TPlugInManager.Create(nil);
  try
    { set filename to our plugin }
    FileName := PluginPath;
    try
      { try to load the plugin }
      isLoaded := PlugInManager.LoadPlugIn(FileName, ISamplePlugIn, SamplePlugIn);
      if not isLoaded then
        raise Exception.CreateFmt(RS_PLUGIN_ERROR, [ExtractFileName(FileName)]);

      { CALL OUR CORE METHODS }
      ShowMessage(IntToStr(SamplePlugIn.Sum(10, 5)));
    except
      on e: Exception do
        MessageBox(0, PChar(e.message), PAnsiChar(CS_NAME),
          MB_ICONERROR);
    end;
    if isLoaded then
      PlugInManager.UnloadPlugIn(SamplePlugIn);
  finally // wrap up
    PlugInManager.Free;
  end;    // try/finally
end;

procedure TFSampleClient.btnLoadPluginClick(Sender: TObject);
begin
  Self.LoadPlugin(ExtractFilePath(ParamStr(0)) + 'libSamplePlugIn.plg');
end;

procedure TFSampleClient.btnLoadPluginXEClick(Sender: TObject);
begin
  Self.LoadPlugin('Win32\Debug\pLibSamplePlugInXE.plg');
end;

end.


