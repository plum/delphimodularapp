object FSampleClient: TFSampleClient
  Left = 813
  Top = 361
  Width = 280
  Height = 108
  Caption = 'FSampleClient'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnLoadPlugin: TButton
    Left = 8
    Top = 8
    Width = 257
    Height = 25
    Caption = 'Load plugin Delphi 5'
    TabOrder = 0
    OnClick = btnLoadPluginClick
  end
  object btnLoadPluginXE: TButton
    Left = 8
    Top = 40
    Width = 257
    Height = 25
    Caption = 'Load plugin Delphi XE'
    TabOrder = 1
    OnClick = btnLoadPluginXEClick
  end
end
