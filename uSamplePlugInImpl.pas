unit uSamplePlugInImpl;

interface

uses
  uPlugInIntf, uSamplePlugInIntf, Classes;

type
{ TSamplePlugIn }

  TSamplePlugIn = class(TPlugInClass, ICustomPlugIn, ISamplePlugIn)
  private
    { Private-Deklarationen }
    List: TStringList;
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetAuthor: string;
    function GetName: string;
    function GetVersion: string;
    function GetDescription: string;
    function Sum(a, b: Integer): Integer;
  end;

implementation

{ TSamplePlugIn }

constructor TSamplePlugIn.Create(AOwner: TComponent);
begin
  List := TStringList.Create;
  List.Add('Created TSamplePlugIn Delphi 5');
  List.SaveToFile('D:\PluginDelphi5.txt');
  inherited Create(AOwner);
end;

destructor TSamplePlugIn.Destroy;
begin
  List.Free;
  inherited;
end;

function TSamplePlugIn.GetAuthor: string;
begin
  Result := 'Marc Hoffmann';
end;

function TSamplePlugIn.GetName: string;
begin
  Result := CS_NAME;
end;

function TSamplePlugIn.GetDescription: string;
begin
  Result := 'Place_your_description_here';
end;

function TSamplePlugIn.GetVersion: string;
begin
  Result := CS_VERSION;
end;

function TSamplePlugIn.Sum(a, b: Integer): Integer;
begin
  Result := a + b;
end;

//procedure TSamplePlugIn.DisplayMessage(const MyText: string);
//begin
//  ShowMessage(MyText);
//end;

//function TSamplePlugIn.ShowWindow(AText: string; AOwner: TComponent): TForm;
//var
//  fPluginForm: TForm;
//begin
//  fPluginForm := TForm.Create(AOwner);
//  fPluginForm.Caption := AText;
//  Result := fPluginForm;
//end;

end.
