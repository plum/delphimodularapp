unit uPlugInMng;

interface

uses
  Windows, Classes, uPlugInIntf;

type
{ TPlugInStruct }

  TPlugInStruct = packed record
    AClass: TPlugInClass;
    GUID: TGUID;
    Handle: THandle;
    AInterface: Pointer;
    FileName: string;
  end;
  PPlugInStruct = ^TPlugInStruct;

{ TPlugInManager }

  TPlugInManager = class(TComponent)
  private
    FPlugIns: TList;
    FLastError: string;
    FOwner: TComponent;
    function GetPlugIn(Index: Integer): PPlugInStruct;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetLastError: string;
    function LoadPlugIn(const AFileName: string; PlugInGUID: TGUID;
      out PlugInIntf; ForceCreate: Boolean = False): Boolean;
    function UnloadPlugIn(var PlugInIntf): Boolean;
    //function FindPlugIn(var PlugInIntf): PPlugInStruct;
    function Count: Integer;
    property PlugIns[Index: Integer]: PPlugInStruct read GetPlugIn;
  end;

implementation

uses
  SysUtils;

{ TPlugInManager }

constructor TPlugInManager.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FOwner := AOwner;
  FLastError := '';
  FPlugIns := TList.Create;
end;

destructor TPlugInManager.Destroy;
begin
  FPlugIns.Free;
  inherited;
end;

function TPlugInManager.GetLastError: string;
begin
  Result := FLastError;
end;

function TPlugInManager.LoadPlugIn(const AFileName: string;
  PlugInGUID: TGUID; out PlugInIntf; ForceCreate: Boolean = False): Boolean;
var
  FileName: string;
  DLLHandle: THandle;
  FuncPtr: TFarProc;
  PlugInProc: TPlugInProc;
  PlugInClass: TPlugInClass;
  PlugInStruct: PPlugInStruct;
begin
  { initialize variables }
  Result := False;
  FileName := AFileName;
  DLLHandle := 0;
  try
    { try to load passed dll }
    // Delphi XE
    //DLLHandle := LoadLibraryW(PWideChar(AFileName));
    DLLHandle := LoadLibrary(PAnsiChar(AFileName));
    if DLLHandle <> 0 then
    begin
      { get function address of 'RegisterPlugIn' }
      FuncPtr := GetProcAddress(DLLHandle, 'RegisterPlugIn');
      if FuncPtr <> nil then
      begin
        { assign register method }
        @PlugInProc := FuncPtr;

        { create plugin instance }
        PlugInClass := TPlugInClass(PlugInProc(FOwner, ForceCreate));  // creates instance!
        { the only tricky-part: accessing the common interface }
        if Assigned(PlugInClass) then begin
          if (PlugInClass.GetInterface(PlugInGUID, PlugInIntf)) then
          begin
            { save plugin properties }
            New(PlugInStruct);
            PlugInStruct.AClass := PlugInClass;
            PlugInStruct.GUID := PlugInGUID;
            PlugInStruct.Handle := DLLHandle;
            PlugInStruct.AInterface := Pointer(PlugInIntf);
            PlugInStruct.FileName := AFileName;
            FPlugIns.Add(PlugInStruct);
            Result := True;
          end;
        end;

        if Result = False then begin
          FreeLibrary(DLLHandle);
        end;
      end;
    end;    // try/finally
  except
    on e: Exception do
    begin
      FLastError := e.Message;
      if DLLHandle <> 0 then
        FreeLibrary(DLLHandle);
    end;
  end;    // try/except
end;

function TPlugInManager.UnloadPlugIn(var PlugInIntf): Boolean;
var
  i: Integer;
  PlugInStruct: PPlugInStruct;
begin
  Result := False;
  try
    for i := FPlugIns.Count - 1 downto 0 do
    begin
      PlugInStruct := FPlugIns[i];
      if PlugInStruct^.AInterface = Pointer(PlugInIntf) then
      begin
        { tricky: beware of the right order!!! }
        Pointer(PlugInIntf) := nil;
        PlugInStruct.AClass.Free;
        FreeLibrary(PlugInStruct^.Handle);
        Dispose(PlugInStruct);
        FPlugIns.Delete(i);
        Result := True;
      end;
    end;
  except
    raise;
  end;
end;

function TPlugInManager.Count: Integer;
begin
  Result := FPlugIns.Count;
end;

function TPlugInManager.GetPlugIn(Index: Integer): PPlugInStruct;
begin
  Result := nil;
  if Index = FPlugIns.Count - 1 then
    Exit;
  Result := PPlugInStruct(FPlugIns[Index]);
end;

end.
