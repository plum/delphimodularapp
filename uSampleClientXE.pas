unit uSampleClientXE;

interface

uses
  System.ShareMem,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  uPlugInMng,
  uSamplePlugInIntf;

type
  TFSimpleClientXE = class(TForm)
    btnLoadDelphi5Plugin: TButton;
    btnLoadDelphiXEPlugin: TButton;
    procedure btnLoadDelphiXEPluginClick(Sender: TObject);
    procedure btnLoadDelphi5PluginClick(Sender: TObject);
  private
    { Private declarations }
    procedure LoadPlugin(PluginPath: string);
  public
    { Public declarations }
  end;

var
  FSimpleClientXE: TFSimpleClientXE;

  PlugInManager: TPlugInManager;
  SamplePlugIn: ISamplePlugIn;
  FileName: string;
  isLoaded: Boolean;

resourcestring
    RS_PLUGIN_ERROR =
      'Error while loading plugin "%s"!';

implementation

{$R *.dfm}

procedure TFSimpleClientXE.LoadPlugin(PluginPath: string);
var fPluginForm: TForm;
begin
  { initialize variables }
  isLoaded := False;

  { create plugin manager }
  PlugInManager := TPlugInManager.Create(nil);
  try
    { set filename to our plugin }
    FileName := PluginPath;
    try
      { try to load the plugin }
      isLoaded := PlugInManager.LoadPlugIn(FileName, ISamplePlugIn, SamplePlugIn);
      if not isLoaded then
        raise Exception.CreateFmt(RS_PLUGIN_ERROR, [ExtractFileName(FileName)]);

      { CALL OUR CORE METHODS }
      ShowMessage(IntToStr(SamplePlugIn.Sum(10, 5)));
//      fPluginForm := SamplePlugIn.ShowWindow('Test', Self);
//      fPluginForm.ShowModal;
//      fPluginForm.Free;
    except
      on e: Exception do
        MessageBox(0, PChar(e.message), PWideChar(CS_NAME),
          MB_ICONERROR);
    end;
    if isLoaded then
      PlugInManager.UnloadPlugIn(SamplePlugIn);
  finally // wrap up
    ShowMessage(PlugInManager.GetLastError);
    PlugInManager.Free;
  end;    // try/finally
end;


procedure TFSimpleClientXE.btnLoadDelphi5PluginClick(Sender: TObject);
begin
  Self.LoadPlugin('C:\Users\Lukasz\Documents\TOP_Projekty\ModularLearning\MyModularProject\libSamplePlugIn.plg');
end;

procedure TFSimpleClientXE.btnLoadDelphiXEPluginClick(Sender: TObject);
begin
  Self.LoadPlugin('C:\Users\Lukasz\Documents\TOP_Projekty\ModularLearning\MyModularProject\Win32\Debug\pLibSamplePlugInXE.plg');
end;


end.
