unit uSamplePlugInImplXE;

interface

uses
  uPlugInIntf, uSamplePlugInIntfXE, Classes, Vcl.Forms;

type
{ TSamplePlugIn }

  TSamplePlugIn = class(TPlugInClass, ICustomPlugIn, ISamplePlugInXE)
  private
    { Private-Deklarationen }
    List: TStringList;
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetAuthor: string;
    function GetName: string;
    function GetVersion: string;
    function GetDescription: string;
    function Sum(a, b: Integer): Integer;
    procedure DisplayMessage(const MyText: string);
    function ShowWindow(AText: string; AOwner: TCOmponent): TForm;
  end;

implementation

uses
  Vcl.Dialogs;

{ TSamplePlugIn }

constructor TSamplePlugIn.Create(AOwner: TComponent);
begin
  List := TStringList.Create;
  List.Add('TSamplePlugIn from Delphi XE2 created!');
  List.SaveToFile('D:\PluginDelphiXE.txt');
  inherited Create(AOwner);
end;

destructor TSamplePlugIn.Destroy;
begin
  List.Free;
  inherited;
end;

function TSamplePlugIn.GetAuthor: string;
begin
  Result := 'Marc Hoffmann';
end;

function TSamplePlugIn.GetName: string;
begin
  Result := CS_NAME;
end;

function TSamplePlugIn.GetDescription: string;
begin
  Result := 'Delphi XE2';
end;

function TSamplePlugIn.GetVersion: string;
begin
  Result := CS_VERSION;
end;

function TSamplePlugIn.ShowWindow(AText: string; AOwner: TCOmponent): TForm;
var
  fPluginForm: TForm;
begin
  fPluginForm := TForm.Create(AOwner);
  fPluginForm.Caption := AText;
  Result := fPluginForm;
end;

function TSamplePlugIn.Sum(a, b: Integer): Integer;
begin
  Result := a + b;
end;

procedure TSamplePlugIn.DisplayMessage(const MyText: string);
begin
  ShowMessage('Delphi XE2 '+ MyText);
end;

end.
